package pl.com.sda.homework.exercisePets;

public class Dog extends Pet {

    public Dog(String covering) {
        super(covering);
    }

    @Override
    public void makeSound() {
        System.out.println("HauHau!");
    }

    @Override
    public void getCountOfLegs() {

    }
}
