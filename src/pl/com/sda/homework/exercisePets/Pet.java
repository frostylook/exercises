package pl.com.sda.homework.exercisePets;

public abstract class Pet{

    protected String covering;

    public Pet(String covering) {
        this.covering = covering;
    }

    public String getCovering(){
        return covering;
    }

    public abstract void makeSound();

    public abstract void getCountOfLegs();
}
